export class Calculator {
    add(a: number, b: number) : number {
        return a + b;
    }

    substract(a: number, b: number) : number {
        return a - b;
    }
}